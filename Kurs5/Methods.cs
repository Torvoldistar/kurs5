﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs5
{
    class Methods
    {
        public double e;
        private const double hd = 0.001;
        private double t1 = (Math.Sqrt(5) - 1) / 2;
        private double t2 = (3 - Math.Sqrt(5.0)) / 2;
        private Parser parser;
        //Конструктор
        public Methods(double e, string expression, int argsCount)
        {
            this.e = e;
            ParserInit(expression, argsCount);
        }
        //Инициализация парсера
        public void ParserInit(string expression,int argsCount)
        {
            parser = new Parser(new Dictionary<string, double>());
            parser.AddFunc("MainMethod",argsCount,expression);
        }
        //Заданная функция
        private double y (List<double> vector)
        {
            var args = new Dictionary<string, double>();
            for ( int i = 0; i<vector.Count;i++)
            {
                args.Add($"a{i}", vector[i]);
            }
            parser.Args = args;
            var expression = new StringBuilder();
            expression.Append("MainMethod(");
            for (int i=0;i<vector.Count-1;i++)
            {
                expression.Append($"a{i},");
            }
            expression.Append($"a{vector.Count-1})");
            return parser.CalcFunction(expression.ToString());
        }
        //Конвертор одномерной минимизации к многомерной(возвращает новую точку)
        private List<double> New(double A, List<double> p, List<double> X0)
        {
            List<double> res = new List<double>();
            for (int i=0;i<X0.Count;i++)
            {
                res.Add(X0[i] + A * p[i]);
            }
            return res;
        }
        //значение функции в точке по направлению
        private double f(double A, List<double> p, List<double> X0)
        {
            return y(New(A, p, X0));
        }
        //Рассчёт градиента
        private List<double> grad(List<double> vector)
        {
            List<double> res = new List<double>();
            for (int i=0; i< vector.Count;i++)
            {
                List<double> temp1 = new List<double>(vector);
                List<double> temp2 = new List<double>(vector);
                List<double> temp3 = new List<double>(vector);
                List<double> temp4 = new List<double>(vector);
                temp1[i] += 2 * hd;
                temp2[i] += hd;
                temp3[i] -= hd;
                temp4[i] -= 2 * hd;
                res.Add((-y(temp1) + 8 * y(temp2) - 8 * y(temp3) + y(temp4)) / (12 * hd));
            }

            return res;
        }
        //Взятие производной в точке по направлению
        private double df( double A, List<double> p, List<double> X0)
        {
            List<double> g = grad(New(A, p, X0));
            double sum=0;
            for (int i =0; i<g.Count;i++)
            {
                sum += g[i] * p[i];
            }
            return sum;
        }
        //Рассчёт нормы вектора
        private double norma(List<double> vector)
        {
            double sum = 0;
            for (int i = 0;i<vector.Count;i++)
            {
                sum += vector[i]*vector[i];
            }
            return Math.Sqrt(sum);
        }
        //Метод Свенн-2
        double[] Swann(List<double> x, List<double> p)
        {
            double[] ab = new double[2];
            ab[0] = norma(x);
            double h = 0.01 * ab[0];
            if (h == 0) h = 0.01;
            if (df(ab[0], p, x) > 0) h = -h;
            ab[1] = ab[0] + h;
            while (f(ab[1], p, x) < f(ab[0], p, x))
            {
                h = 2 * h;
                ab[0] = ab[1];
                ab[1] = ab[0] + h;
            }
            ab[0] = ab[0] - h / 2;
            if (h < 0) { ab[0] = ab[0] + ab[1]; ab[1] = ab[0] - ab[1]; ab[0] = ab[0] - ab[1]; }
            return ab;
        }
        //Рассчёт длины отрезка
        double Length(double a, double b)
        {
            return Math.Abs(b-a);
        }
        //Метод Золотон Сечение - 1
        double[] ZS1(double[] ab, List<double> p, List<double> x)
        {
            int kZS1 = 0;
            double Lk = Length(ab[0], ab[1]);
            double mk = ab[0] + t1 * Lk;
            double lk = ab[0] + t2 * Lk;
            double[] akbk = new double[2] {ab[0], ab[1]};
            while ((Lk > e))
            {
                if (f(lk, p, x) < f(mk, p, x)) { akbk[1] = mk; mk = lk; Lk = Length(akbk[0], akbk[1]); lk = akbk[0] + t2 * Lk; }
                else { akbk[0] = lk; lk = mk; Lk = Length(akbk[0], akbk[1]); mk = akbk[0] + t1 * Lk; }
                if (kZS1 == 5) break;
                kZS1++;
            }
            return akbk;
        }
        //Метод ДСК
        double DSK(double[] ab, List<double> p, List<double> x)
        {
            double[] akbk = new double [2]{ ab[0], ab[1] };
            double h = 0.02;
            int kDSK = 1;
            double b, d, hs, x1, x2;
            if (f(akbk[0] + (akbk[1] - akbk[0]) / 3, p, x) > f(akbk[0] + 2 * (akbk[1] - akbk[0]) / 3, p, x)) { b = akbk[0] + 2 * (akbk[1] - akbk[0]) / 3; akbk[0] = akbk[0] + (akbk[1] - akbk[0]) / 3; }
            else { b = akbk[0] + (akbk[1] - akbk[0]) / 3; akbk[1] = akbk[0] + 2 * (akbk[1] - akbk[0]) / 3; }
            d = b + 0.5 * ((b - akbk[0]) * (b - akbk[0]) * (f(b, p, x) - f(akbk[1], p, x)) - (b - akbk[1]) * (b - akbk[1]) * (f(b, p, x) - f(akbk[0], p, x))) / ((b - akbk[0]) * (f(b, p, x) - f(akbk[1], p, x)) - (b - akbk[1]) * (f(b, p, x) - f(akbk[0], p, x)));
            while (((Math.Abs((b - d) / b) > e) || (Math.Abs((f(b, p, x) - f(d, p, x)) / f(b, p, x)) > e))&&(kDSK<40))
            {
                h = h / 2;
                hs = h;
                if (f(b, p, x) > f(d, p, x)) x1 = d;
                else x1 = b;
                if (df(x1, p, x) > 0) hs = -hs;
                x2 = x1 + hs;
                while (f(x2, p, x) < f(x1, p, x))
                {
                    hs = 2 * hs;
                    x1 = x2;
                    x2 = x1 + hs;
                }
                x1 = x1 - hs / 2;
                if (h < 0) { akbk[0] = x2; akbk[1] = x1; }
                else { akbk[0] = x1; akbk[1] = x2; }
                if (f(akbk[0] + (akbk[1] - akbk[0]) / 3, p, x) > f(akbk[0] + 2 * (akbk[1] - akbk[0]) / 3, p, x)) { b = akbk[0] + 2 * (akbk[1] - akbk[0]) / 3; akbk[0] = akbk[0] + (akbk[1] - akbk[0]) / 3; }
                else { b = akbk[0] + (akbk[1] - akbk[0]) / 3; akbk[1] = akbk[0] + 2 * (akbk[1] - akbk[0]) / 3; }
                d = b + 0.5 * ((b - akbk[0]) * (b - akbk[0]) * (f(b, p, x) - f(akbk[1], p, x)) - (b - akbk[1]) * (b - akbk[1]) * (f(b, p, x) - f(akbk[0], p, x))) / ((b - akbk[0]) * (f(b, p, x) - f(akbk[1], p, x)) - (b - akbk[1]) * (f(b, p, x) - f(akbk[0], p, x)));
                kDSK++;
            }
            return (b + d) / 2;
        }
        //Метод, осуществляющий поиск коэф-та направленя А(alpha)
        //lab3
        double AlphaSearch(List<double> x, List<double> p)
        {
            return DSK(ZS1(Swann(x, p), p, x), p, x);
        }
        //Формула Даниэла
        double DanielForm(List<double> currentg, List<double> prevg, List<double> p)
        {
            double result1 = 0;
            double result2 = 0;
            for (int i=0;i<p.Count;i++)
            {
                result1 += currentg[i] * (currentg[i] - prevg[i]);
                result2 += p[i] * (currentg[i] - prevg[i]);
            }
            return result1/result2;
        }
        //Метод Даниэла
        public List<double> Daniel(List<double> X0)
        {
            List<double> currentg = grad(X0);
            List<double> prevg = new List<double>();
            List<double> p = new List<double>();
            List<double> x = new List<double>(X0);
            int kDen = 0;
            double alpha;
            for (int i=0;i<X0.Count;i++)
            {
                p.Add(0);
                prevg.Add(0);
            }
            do
            {
                kDen++;
                if (kDen % 2 == 1)
                    for( int i=0;i<x.Count;i++)
                    {
                        p[i] = -currentg[i];
                    }
                else
                    for (int i = 0; i < x.Count; i++)
                    {
                        p[i] = -currentg[i] + p[i]*DanielForm(currentg,prevg,p);
                    }
                alpha = AlphaSearch(x, p);
                for (int i = 0; i < x.Count; i++)
                {
                    x[i] += alpha*p[i];
                }
                prevg = currentg;
                currentg = grad(x);
            } while (norma(currentg) > e);
            return x;
        }
    }
}
