﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs5
{
    class Function
    {
        public delegate double Func(params double[] args);//функция от неизвестного числа аргументов в общем виде

        public int ArgsCount { get; set; }//число аргументов
        public Func func;//Функция 

        public Function(Func func, int argsCount)
        {
            this.func = func;
            ArgsCount = argsCount;
        }
    }
}
