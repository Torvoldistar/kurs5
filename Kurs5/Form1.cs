﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kurs5
{
    public partial class Form1 : Form
    {
        Methods Daniel;//Метод оптимизации
        List<double> Args;//количество аргументов функции
        public Form1()
        {
            InitializeComponent();
            textBox1.Text = "4 * (arg0 - 5) * (arg0 - 5) + (arg1 - 6) * (arg1 - 6)";
            label3.Visible = false;
            label4.Visible = false;
            textBox2.Visible = false;
        }
        //Вычислить точку минимума функции
        private void button1_Click(object sender, EventArgs e)
        {
            label5.Text = "";
            textBox2.Text = "";
            try
            {
                label3.Visible = true;
                Daniel = new Methods(0.001, textBox1.Text, dataGridView1.RowCount - 1);
                Args = new List<double>();
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                {
                    Args.Add(double.Parse(dataGridView1[0, i].Value.ToString()));
                }

                label4.Visible = true;
                textBox2.Visible = true;
                var result = Daniel.Daniel(Args);

                for (int i = 0; i < Args.Count - 1; i++)
                {
                    if (double.IsNaN(result[i])) throw new Exception();
                }

                textBox2.Text = "(";
                for (int i = 0; i < Args.Count - 1; i++)
                {
                    textBox2.Text += $"{result[i]:f2};";
                }

                textBox2.Text += $"{result[Args.Count - 1]:f2})";
            }
            catch (ArgumentException ex)
            {
                label5.Text = "Ошибка: Неверно заданные условия";
            }
            catch (Exception ex)
            {
                label5.Text = "Ошибка: Во время вычислений произошла некорректная операция\n               Например деление на ноль";
            }
            
        }

    }
}
